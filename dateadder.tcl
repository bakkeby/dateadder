#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|dateadder|Prefixes files and folders with the file's date
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require dateadder 1.0

cfg set DESTINATION [pwd]
cfg file [file rootname [this]].cfg
cfg set HELP \
{Usage: dateadder [OPTION]... [FILE]...

Prefixes files and folders with the file's date based on the file creation date (or EXIF information if present).

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

Warning! The expiry date predates the outdated use-by date.
}

if {[info exists argv]} {
	dateadder::init
	dateadder::main $argv
}
