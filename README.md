# DateAdder

DateAdder is a relatively simple and trivial script that just prefixes a file with the file's date.

My main reason for implementing this is because a date prefix allows for files to be sorted by time alphanumerically and it also makes it easier to find the file you need. I typically use this for backup files.

This could have also been implemented with a relatively simple bash script. The main benefits for doing this in Tcl is:

   - it's platform independent
   - dry-run option
   - optional logging and configuration
   - option to pull the date from EXIF information if relevant

This script is also an example [TApp](https://bitbucket.org/bakkeby/tapp) application.

-----

## Installation

Grab the file from the [downloads](https://bitbucket.org/bakkeby/dateadder/downloads) section, unpack and run it.

If you clone the source then you will also need to ensure that [TApp](https://bitbucket.org/bakkeby/tapp) is also cloned and is present (as a symlink or otherwise) in the script directory.

-----

## Configuration

No particular configuration is needed to use this script, but if for some reason you feel like overriding the defaults then you can create the *dateadder.cfg* configuration file within the script directory.

If you are using raw Nikon .NEF files and want EXIF information then you may want to consider setting the NEF_EXIF_CMD config item (defaults to "[*exiftool*](http://www.sno.phy.queensu.ca/~phil/exiftool/)").

See [https://bitbucket.org/bakkeby/tapp/wiki/API/file_utils](https://bitbucket.org/bakkeby/tapp/wiki/API/file_utils) for a list of configuration items that can be overridden.

-----

## License

   - Simplified BSD License/FreeBSD License (GPL-compatible free software license)

-----

## Dependencies

   - [TApp](https://bitbucket.org/bakkeby/tapp)
   - [Tcl8.5](https://www.tcl.tk/)
   - [tcllib](http://tcllib.sourceforge.net/)
