# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package provide dateadder 1.0

namespace eval dateadder {}

proc dateadder::process_files { files } {

	foreach file $files {
		set file_name [file tail $file]
		set file_dir  [file dir $file]
		# Add date prefix if -date flag is set, but not if date already exists.
		set date_format [cfg get FILE_DATE_PREFIX_FORMAT {%Y.%m.%d.}]
		set date_regexp [text_utils::convert_clock_format_to_regexp $date_format]
		if {[regexp -- "^$date_regexp" $file_name] == 0} {
			set prefix [determine_file_time $file $date_format]
			set file_name ${prefix}$file_name

			move $file [file join $file_dir $file_name]
		} else {
			log WARN {The file $file already has a date, ignoring}
		}
	}
}

proc dateadder::init {} {

	param register -i --image\
		--config FILE_TIME_BY_EXIF\
		--comment {Enabled use of EXIF information to determine file date}
}

proc dateadder::main { argv } {
	if {[llength $argv] == 0} {
		lappend argv --help
	}

	param parse $argv remaining_args
	param separate $remaining_args app_args files dirs

	process_files [concat $files $dirs]
}